<?php
/**
 * @file
 * drucloud_users.features.user_role.inc
 */

/**
 * Implements hook_user_default_roles().
 */
function drucloud_users_user_default_roles() {
  $roles = array();

  // Exported role: editor.
  $roles['editor'] = array(
    'name' => 'editor',
    'weight' => 4,
  );

  // Exported role: super_editor.
  $roles['super_editor'] = array(
    'name' => 'super_editor',
    'weight' => 3,
  );

  return $roles;
}
