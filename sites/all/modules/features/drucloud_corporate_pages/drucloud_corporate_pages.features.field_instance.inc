<?php
/**
 * @file
 * drucloud_corporate_pages.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function drucloud_corporate_pages_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance: 'entityqueue_subqueue-featured_on-eq_taxonomy_term'
  $field_instances['entityqueue_subqueue-featured_on-eq_taxonomy_term'] = array(
    'bundle' => 'featured_on',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'entityreference',
        'settings' => array(
          'link' => FALSE,
        ),
        'type' => 'entityreference_label',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'entityqueue_subqueue',
    'field_name' => 'eq_taxonomy_term',
    'label' => 'Queue items',
    'required' => FALSE,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'module' => 'entityqueue',
      'settings' => array(
        'match_operator' => 'CONTAINS',
        'size' => 60,
      ),
      'type' => 'entityqueue_dragtable',
      'weight' => 0,
    ),
  );

  // Exported field_instance: 'taxonomy_term-media-field_media_image'
  $field_instances['taxonomy_term-media-field_media_image'] = array(
    'bundle' => 'media',
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'image',
        'settings' => array(
          'image_link' => '',
          'image_style' => '',
        ),
        'type' => 'image',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'taxonomy_term',
    'field_name' => 'field_media_image',
    'label' => 'Media image',
    'required' => 1,
    'settings' => array(
      'alt_field' => 1,
      'default_image' => 0,
      'file_directory' => 'media',
      'file_extensions' => 'png gif jpg jpeg',
      'image_field_caption' => 0,
      'max_filesize' => '',
      'max_resolution' => '',
      'min_resolution' => '',
      'title_field' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'image',
      'settings' => array(
        'insert' => 0,
        'insert_absolute' => 0,
        'insert_class' => '',
        'insert_default' => 'auto',
        'insert_styles' => array(
          'auto' => 'auto',
          'icon_link' => 0,
          'image' => 0,
          'image_activity_stream_article' => 0,
          'image_activity_stream_comment' => 0,
          'image_article_author' => 0,
          'image_article_header' => 0,
          'image_article_header_mobile' => 0,
          'image_article_related_news' => 0,
          'image_article_teaser' => 0,
          'image_comment_image' => 0,
          'image_comment_user_picture' => 0,
          'image_community_box_teaser' => 0,
          'image_community_submitted_module' => 0,
          'image_community_teaser' => 0,
          'image_feature_article_image' => 0,
          'image_homepage_featured_article' => 0,
          'image_homepage_hero' => 0,
          'image_homepage_sponsored_article' => 0,
          'image_juicebox_gallery' => 0,
          'image_juicebox_thumb' => 0,
          'image_large' => 0,
          'image_medium' => 0,
          'image_mobile_profile_page_user_picture' => 0,
          'image_new_comment_user_image' => 0,
          'image_profile_page_user_picture' => 0,
          'image_related_article_header' => 0,
          'image_square_thumbnail' => 0,
          'image_story_stream' => 0,
          'image_story_stream_mobile' => 0,
          'image_story_stream_tablet' => 0,
          'image_teasers_menu' => 0,
          'image_thumbnail' => 0,
          'image_trending_articles' => 0,
          'link' => 0,
        ),
        'insert_width' => '',
        'preview_image_style' => 'thumbnail',
        'progress_indicator' => 'throbber',
      ),
      'type' => 'image_image',
      'weight' => 41,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Media image');
  t('Queue items');

  return $field_instances;
}
