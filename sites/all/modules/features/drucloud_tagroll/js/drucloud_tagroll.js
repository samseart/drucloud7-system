(function ($) {

  Drupal.drucloud_tagroll = Drupal.drucloud_tagroll || {};
  Drupal.drucloud_tagroll.behaviors = Drupal.drucloud_tagroll.behaviors || {};
  Drupal.drucloud_tagroll.data = Drupal.drucloud_tagroll.data || {};

  /**
   * Add all handlers.
   */
  Drupal.behaviors.drucloud_tagroll = {
    attach: function(context, settings) {
      $(function () {
        // Init data.
        Drupal.drucloud_tagroll.data.primary_menu = $('#block-views-tagroll-primary-tagroll', context);
        Drupal.drucloud_tagroll.data.secondary_menu = $('#block-views-tagroll-secondary-tagroll', context);
        Drupal.drucloud_tagroll.data.teasers_url = 'drucloud-tagroll/teasers-view/';
        Drupal.drucloud_tagroll.data.secondary_menu.hide();

        // Attach all drucloud_tagroll behaviours.
        Drupal.drucloud_tagroll.attachBehaviors();
      });
    }
  };

  /**
   * Attach all drucloud_tagroll behaviors.
   */
  Drupal.drucloud_tagroll.attachBehaviors = function (context, settings) {
    $.each(Drupal.drucloud_tagroll.behaviors, function() {
      this(context, settings);
    });
  };

  /**
   * Menu behaviour.
   */
  Drupal.drucloud_tagroll.behaviors.menu_behaviour = function (context, settings) {
    var $primary_menu = Drupal.drucloud_tagroll.data.primary_menu;
    var $secondary_menu = Drupal.drucloud_tagroll.data.secondary_menu;
    var $target = $('#block-views-tagroll-secondary-tagroll .secondary-tagroll-teasers');

    // Processing of primary menu.
    $primary_menu.once().find('.filter-field a')
      .removeClass('primary-active-filer')
      .mouseenter(function(event) {
        $('.bg-overlay').css('width', 0);
        $primary_menu.find('.filter-field a').removeClass('primary-active-filer');
        var primary_tid = $(this).addClass('primary-active-filer').addClass('primary-active-link').find('.tid-source').data('tid');
        var primary_name = $(this).find('.tid-source').data('name');
        var link = $(this);

        // If exist tid load teasers.
        if (primary_tid !== 'undefined' && link.hasClass('primary-active-link')) {
          $target.html('<div class="placeholder placeholder-spinner"></div>');
          $secondary_menu.show().stop().animate({'margin-top': 0}, 100, 'easeOutExpo');
          var url = Drupal.settings.basePath + Drupal.drucloud_tagroll.data.teasers_url + primary_tid;
          Drupal.drucloud_tagroll.run_query(context, url, $target, 'replace');
        }

        // Replace original links with double filter.
        $secondary_menu.find('.secondary-tagroll-menu a').each(function() {
            if($(this).attr('href')=='/tags/community') {    
              $(this).attr('href', "/community");
            }
        });
      })
      .mouseleave(function(event) {
        $(this).removeClass('primary-active-link');
      });

    // Processing of secondary menu.
    $secondary_menu.once().find('.filter-field a')
      .removeClass('secondary-active-filer')
      .mouseenter(function(event) {
        $secondary_menu.find('.filter-field a').removeClass('secondary-active-filer');
        $('.nav-tabs > ul li').trigger('mouseenter.slide');
        var primary_tid = $('.primary-active-filer').find('.tid-source').data('tid');
        var secondary_tid = $(this).addClass('secondary-active-filer').addClass('secondary-active-link').find('span').data('tid');
        var link = $(this);
        if (primary_tid !== 'undefined' && secondary_tid !== 'undefined' && link.hasClass('secondary-active-link')) {
          $target.html('<div class="placeholder placeholder-spinner"></div>');
          var url = Drupal.settings.basePath + Drupal.drucloud_tagroll.data.teasers_url + secondary_tid;
          Drupal.drucloud_tagroll.run_query(context, url, $target, 'replace');
        }
      })
      .mouseleave(function(event) {
        $(this).removeClass('secondary-active-link');
      });

    // Hide secondary tagroll when mouser leave.
    $('#block-views-tagroll-primary-tagroll, .secondary-tagroll-popup')
      .mouseenter(function(event) {
        $secondary_menu.removeClass('hide-menu');
      })
      .mouseleave(function(event) {
        $secondary_menu.addClass('hide-menu');
        $secondary_menu.find('.filter-field a').removeClass('secondary-active-filer active');
        setTimeout(function(){
          $('.hide-menu').stop().animate({'margin-top': -300}, 50, 'easeOutExpo');
        }, 100);
      });
    $('#block-views-tagroll-primary-tagroll li:last-child')
      .mouseenter(function(event) {
        $secondary_menu.addClass('hide-menu');
        setTimeout(function(){
          $('.hide-menu').stop().animate({'margin-top': -300}, 50, 'easeOutExpo');
        }, 100);
    }) 
  };

  /**
   * Run ajax query.
   */
  Drupal.drucloud_tagroll.run_query = function (context, url, $target, mode) {
    var output = Drupal.drucloud_tagroll.local_storage_load(url);
    if (!output) {
      $.ajax({
        type: 'GET',
        url: url,
        async: false,
        dataType: 'html',
        success: function (output) {
          // Expire after 30 minutes.
          Drupal.drucloud_tagroll.insert_output(context, $target, mode, output);
          Drupal.drucloud_tagroll.local_storage_save(url, output, 30);
        }
      });
    }
    else {
      Drupal.drucloud_tagroll.insert_output(context, $target, mode, output);
    }
  };

  Drupal.drucloud_tagroll.prefetch_query = function (context, url, mode) {
    var output = Drupal.drucloud_tagroll.local_storage_load(url);

    if (!output) {
      $.ajax({
        type: 'GET',
        url: url,
        async: false,
        dataType: 'html',
        success: function (output) {
          Drupal.drucloud_tagroll.local_storage_save(url, output, 30);
        }
      });
   }

   $('body').append('<div id="preload"></div>');  
   $('#preload').html(output).hide();              

  var images = [];
    $("#preload img").each(function(){
//  alert($(this).attr('src'));
  $('<img/>')[0].src = $(this).attr('src');
    })
};


  /**
   * Insert output to markup.
   */
  Drupal.drucloud_tagroll.insert_output = function (context, $target, mode, output) {
    // Replace old data.
    if (mode == 'replace') {
//      $target.html(output).find('.tab-content').hide().fadeIn(1500);
//       $target.html(output);

      $(".teasers-view").html(output);

    }
    // Add new data.
    if (mode == 'add') {
//      $target.append(output).find('.tab-content').hide().fadeIn(1500);
//        $target.append(output);
      $(".teasers-view").append(output);

    }
  };

  /**
   * Save data to local storage.
   */
  Drupal.drucloud_tagroll.local_storage_save = function(key, jsonData, expirationMin){
    if (!Modernizr.localstorage){return false;}
    var expirationMS = expirationMin * 60 * 1000;
    var record = {value: JSON.stringify(jsonData), timestamp: new Date().getTime() + expirationMS};
    localStorage.setItem(key, JSON.stringify(record));
    return jsonData;
  };

  /**
   * Load data from local storage.
   */
  Drupal.drucloud_tagroll.local_storage_load = function(key){
    if (!Modernizr.localstorage){return false;}
    var record = JSON.parse(localStorage.getItem(key));
    if (!record){return false;}
    return (new Date().getTime() < record.timestamp && JSON.parse(record.value));
  };


  $(document).ready(function() {

//Enumerate the main menu items and preload as soon as the DOM has finished processing
  var swidth = $(window).width();
  if(swidth > 767) {
  $("#header .tid-source").each(function() { 
     var value = $(this).data('tid');
      var base_url = "/drucloud-tagroll/teasers-view/";
      var url = base_url+value;
      Drupal.drucloud_tagroll.prefetch_query('', url, 'replace');
  });
  }
  });

})(jQuery);
